export var containerStyleDefault = {
    position: 'relative',
    overflow: 'hidden',
    width: '100%',
    height: '100%'
};

// Overrides containerStyleDefault properties
export var containerStyleAutoHeight = {
    height: 'auto'
};

export var viewStyleDefault = {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: 'scroll',
    WebkitOverflowScrolling: 'touch'
};

// Overrides viewStyleDefault properties
export var viewStyleAutoHeight = {
    position: 'relative',
    top: undefined,
    left: undefined,
    right: undefined,
    bottom: undefined
};

export var viewStyleUniversalInitial = {
    overflow: 'hidden',
    marginRight: 0,
    marginBottom: 0
};

export var trackHorizontalStyleDefault = {
    position: 'absolute',
    height: 6
};

export var trackVerticalStyleDefault = {
    position: 'absolute',
    width: 6
};

export var thumbHorizontalStyleDefault = {
    position: 'relative',
    display: 'block',
    height: '100%'
};

export var thumbVerticalStyleDefault = {
    position: 'relative',
    display: 'block',
    width: '100%'
};

export var disableSelectStyle = {
    userSelect: 'none'
};

export var disableSelectStyleReset = {
    userSelect: ''
};