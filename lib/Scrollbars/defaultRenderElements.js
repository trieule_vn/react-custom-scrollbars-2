var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

import React from 'react';
/* eslint-disable react/prop-types */

export function renderViewDefault(props) {
    return React.createElement('div', props);
}

export function renderTrackHorizontalDefault(_ref) {
    var { style } = _ref,
        props = _objectWithoutProperties(_ref, ['style']);

    var finalStyle = _extends({}, style, {
        right: 2,
        bottom: 2,
        left: 2,
        borderRadius: 3
    });
    return React.createElement('div', _extends({ style: finalStyle }, props));
}

export function renderTrackVerticalDefault(_ref2) {
    var { style } = _ref2,
        props = _objectWithoutProperties(_ref2, ['style']);

    var finalStyle = _extends({}, style, {
        right: 2,
        bottom: 2,
        top: 2,
        borderRadius: 3
    });
    return React.createElement('div', _extends({ style: finalStyle }, props));
}

export function renderThumbHorizontalDefault(_ref3) {
    var { style } = _ref3,
        props = _objectWithoutProperties(_ref3, ['style']);

    var finalStyle = _extends({}, style, {
        cursor: 'pointer',
        borderRadius: 'inherit',
        backgroundColor: 'rgba(0,0,0,.2)'
    });
    return React.createElement('div', _extends({ style: finalStyle }, props));
}

export function renderThumbVerticalDefault(_ref4) {
    var { style } = _ref4,
        props = _objectWithoutProperties(_ref4, ['style']);

    var finalStyle = _extends({}, style, {
        cursor: 'pointer',
        borderRadius: 'inherit',
        backgroundColor: 'rgba(0,0,0,.2)'
    });
    return React.createElement('div', _extends({ style: finalStyle }, props));
}