var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

import raf, { cancel as caf } from 'raf';
import css from 'dom-css';
import { Component, createElement, cloneElement } from 'react';
import PropTypes from 'prop-types';

import isString from '../utils/isString';
import getScrollbarWidth from '../utils/getScrollbarWidth';
import returnFalse from '../utils/returnFalse';
import getInnerWidth from '../utils/getInnerWidth';
import getInnerHeight from '../utils/getInnerHeight';

import { containerStyleDefault, containerStyleAutoHeight, viewStyleDefault, viewStyleAutoHeight, viewStyleUniversalInitial, trackHorizontalStyleDefault, trackVerticalStyleDefault, thumbHorizontalStyleDefault, thumbVerticalStyleDefault, disableSelectStyle, disableSelectStyleReset } from './styles';

import { renderViewDefault, renderTrackHorizontalDefault, renderTrackVerticalDefault, renderThumbHorizontalDefault, renderThumbVerticalDefault } from './defaultRenderElements';

export default class Scrollbars extends Component {

    constructor(props, ...rest) {
        super(props, ...rest);

        this.getScrollLeft = this.getScrollLeft.bind(this);
        this.getScrollTop = this.getScrollTop.bind(this);
        this.getScrollWidth = this.getScrollWidth.bind(this);
        this.getScrollHeight = this.getScrollHeight.bind(this);
        this.getClientWidth = this.getClientWidth.bind(this);
        this.getClientHeight = this.getClientHeight.bind(this);
        this.getValues = this.getValues.bind(this);
        this.getThumbHorizontalWidth = this.getThumbHorizontalWidth.bind(this);
        this.getThumbVerticalHeight = this.getThumbVerticalHeight.bind(this);
        this.getScrollLeftForOffset = this.getScrollLeftForOffset.bind(this);
        this.getScrollTopForOffset = this.getScrollTopForOffset.bind(this);

        this.scrollLeft = this.scrollLeft.bind(this);
        this.scrollTop = this.scrollTop.bind(this);
        this.scrollToLeft = this.scrollToLeft.bind(this);
        this.scrollToTop = this.scrollToTop.bind(this);
        this.scrollToRight = this.scrollToRight.bind(this);
        this.scrollToBottom = this.scrollToBottom.bind(this);

        this.handleTrackMouseEnter = this.handleTrackMouseEnter.bind(this);
        this.handleTrackMouseLeave = this.handleTrackMouseLeave.bind(this);
        this.handleHorizontalTrackMouseDown = this.handleHorizontalTrackMouseDown.bind(this);
        this.handleVerticalTrackMouseDown = this.handleVerticalTrackMouseDown.bind(this);
        this.handleHorizontalThumbMouseDown = this.handleHorizontalThumbMouseDown.bind(this);
        this.handleVerticalThumbMouseDown = this.handleVerticalThumbMouseDown.bind(this);
        this.handleWindowResize = this.handleWindowResize.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.handleDragEnd = this.handleDragEnd.bind(this);

        this.state = {
            didMountUniversal: false
        };
    }

    componentDidMount() {
        this.addListeners();
        this.update();
        this.componentDidMountUniversal();
    }

    componentDidMountUniversal() {
        // eslint-disable-line react/sort-comp
        var { universal } = this.props;
        if (!universal) return;
        this.setState({ didMountUniversal: true });
    }

    componentDidUpdate() {
        this.update();
    }

    componentWillUnmount() {
        this.removeListeners();
        caf(this.requestFrame);
        clearTimeout(this.hideTracksTimeout);
        clearInterval(this.detectScrollingInterval);
    }

    getScrollLeft() {
        if (!this.view) return 0;
        return this.view.scrollLeft;
    }

    getScrollTop() {
        if (!this.view) return 0;
        return this.view.scrollTop;
    }

    getScrollWidth() {
        if (!this.view) return 0;
        return this.view.scrollWidth;
    }

    getScrollHeight() {
        if (!this.view) return 0;
        return this.view.scrollHeight;
    }

    getClientWidth() {
        if (!this.view) return 0;
        return this.view.clientWidth;
    }

    getClientHeight() {
        if (!this.view) return 0;
        return this.view.clientHeight;
    }

    getValues() {
        var {
            scrollLeft = 0,
            scrollTop = 0,
            scrollWidth = 0,
            scrollHeight = 0,
            clientWidth = 0,
            clientHeight = 0
        } = this.view || {};

        return {
            left: scrollLeft / (scrollWidth - clientWidth) || 0,
            top: scrollTop / (scrollHeight - clientHeight) || 0,
            scrollLeft,
            scrollTop,
            scrollWidth,
            scrollHeight,
            clientWidth,
            clientHeight
        };
    }

    getThumbHorizontalWidth() {
        var { thumbSize, thumbMinSize } = this.props;
        var { scrollWidth, clientWidth } = this.view;
        var trackWidth = getInnerWidth(this.trackHorizontal);
        var width = Math.ceil(clientWidth / scrollWidth * trackWidth);
        if (trackWidth <= width) return 0;
        if (thumbSize) return thumbSize;
        return Math.max(width, thumbMinSize);
    }

    getThumbVerticalHeight() {
        var { thumbSize, thumbMinSize } = this.props;
        var { scrollHeight, clientHeight } = this.view;
        var trackHeight = getInnerHeight(this.trackVertical);
        var height = Math.ceil(clientHeight / scrollHeight * trackHeight);
        if (trackHeight <= height) return 0;
        if (thumbSize) return thumbSize;
        return Math.max(height, thumbMinSize);
    }

    getScrollLeftForOffset(offset) {
        var { scrollWidth, clientWidth } = this.view;
        var trackWidth = getInnerWidth(this.trackHorizontal);
        var thumbWidth = this.getThumbHorizontalWidth();
        return offset / (trackWidth - thumbWidth) * (scrollWidth - clientWidth);
    }

    getScrollTopForOffset(offset) {
        var { scrollHeight, clientHeight } = this.view;
        var trackHeight = getInnerHeight(this.trackVertical);
        var thumbHeight = this.getThumbVerticalHeight();
        return offset / (trackHeight - thumbHeight) * (scrollHeight - clientHeight);
    }

    scrollLeft(left = 0) {
        if (!this.view) return;
        this.view.scrollLeft = left;
    }

    scrollTop(top = 0) {
        if (!this.view) return;
        this.view.scrollTop = top;
    }

    scrollToLeft() {
        if (!this.view) return;
        this.view.scrollLeft = 0;
    }

    scrollToTop() {
        if (!this.view) return;
        this.view.scrollTop = 0;
    }

    scrollToRight() {
        if (!this.view) return;
        this.view.scrollLeft = this.view.scrollWidth;
    }

    scrollToBottom() {
        if (!this.view) return;
        this.view.scrollTop = this.view.scrollHeight;
    }

    addListeners() {
        /* istanbul ignore if */
        if (typeof document === 'undefined' || !this.view) return;
        var { view, trackHorizontal, trackVertical, thumbHorizontal, thumbVertical } = this;
        view.addEventListener('scroll', this.handleScroll);
        if (!getScrollbarWidth()) return;
        trackHorizontal.addEventListener('mouseenter', this.handleTrackMouseEnter);
        trackHorizontal.addEventListener('mouseleave', this.handleTrackMouseLeave);
        trackHorizontal.addEventListener('mousedown', this.handleHorizontalTrackMouseDown);
        trackVertical.addEventListener('mouseenter', this.handleTrackMouseEnter);
        trackVertical.addEventListener('mouseleave', this.handleTrackMouseLeave);
        trackVertical.addEventListener('mousedown', this.handleVerticalTrackMouseDown);
        thumbHorizontal.addEventListener('mousedown', this.handleHorizontalThumbMouseDown);
        thumbVertical.addEventListener('mousedown', this.handleVerticalThumbMouseDown);
        window.addEventListener('resize', this.handleWindowResize);
    }

    removeListeners() {
        /* istanbul ignore if */
        if (typeof document === 'undefined' || !this.view) return;
        var { view, trackHorizontal, trackVertical, thumbHorizontal, thumbVertical } = this;
        view.removeEventListener('scroll', this.handleScroll);
        if (!getScrollbarWidth()) return;
        trackHorizontal.removeEventListener('mouseenter', this.handleTrackMouseEnter);
        trackHorizontal.removeEventListener('mouseleave', this.handleTrackMouseLeave);
        trackHorizontal.removeEventListener('mousedown', this.handleHorizontalTrackMouseDown);
        trackVertical.removeEventListener('mouseenter', this.handleTrackMouseEnter);
        trackVertical.removeEventListener('mouseleave', this.handleTrackMouseLeave);
        trackVertical.removeEventListener('mousedown', this.handleVerticalTrackMouseDown);
        thumbHorizontal.removeEventListener('mousedown', this.handleHorizontalThumbMouseDown);
        thumbVertical.removeEventListener('mousedown', this.handleVerticalThumbMouseDown);
        window.removeEventListener('resize', this.handleWindowResize);
        // Possibly setup by `handleDragStart`
        this.teardownDragging();
    }

    handleScroll(event) {
        var _this = this;

        var { onScroll, onScrollFrame } = this.props;
        if (onScroll) onScroll(event);
        this.update(function (values) {
            var { scrollLeft, scrollTop } = values;
            _this.viewScrollLeft = scrollLeft;
            _this.viewScrollTop = scrollTop;
            if (onScrollFrame) onScrollFrame(values);
        });
        this.detectScrolling();
    }

    handleScrollStart() {
        var { onScrollStart } = this.props;
        if (onScrollStart) onScrollStart();
        this.handleScrollStartAutoHide();
    }

    handleScrollStartAutoHide() {
        var { autoHide } = this.props;
        if (!autoHide) return;
        this.showTracks();
    }

    handleScrollStop() {
        var { onScrollStop } = this.props;
        if (onScrollStop) onScrollStop();
        this.handleScrollStopAutoHide();
    }

    handleScrollStopAutoHide() {
        var { autoHide } = this.props;
        if (!autoHide) return;
        this.hideTracks();
    }

    handleWindowResize() {
        getScrollbarWidth(false);
        this.forceUpdate();
    }

    handleHorizontalTrackMouseDown(event) {
        event.preventDefault();
        var { target, clientX } = event;
        var { left: targetLeft } = target.getBoundingClientRect();
        var thumbWidth = this.getThumbHorizontalWidth();
        var offset = Math.abs(targetLeft - clientX) - thumbWidth / 2;
        this.view.scrollLeft = this.getScrollLeftForOffset(offset);
    }

    handleVerticalTrackMouseDown(event) {
        event.preventDefault();
        var { target, clientY } = event;
        var { top: targetTop } = target.getBoundingClientRect();
        var thumbHeight = this.getThumbVerticalHeight();
        var offset = Math.abs(targetTop - clientY) - thumbHeight / 2;
        this.view.scrollTop = this.getScrollTopForOffset(offset);
    }

    handleHorizontalThumbMouseDown(event) {
        event.preventDefault();
        this.handleDragStart(event);
        var { target, clientX } = event;
        var { offsetWidth } = target;
        var { left } = target.getBoundingClientRect();
        this.prevPageX = offsetWidth - (clientX - left);
    }

    handleVerticalThumbMouseDown(event) {
        event.preventDefault();
        this.handleDragStart(event);
        var { target, clientY } = event;
        var { offsetHeight } = target;
        var { top } = target.getBoundingClientRect();
        this.prevPageY = offsetHeight - (clientY - top);
    }

    setupDragging() {
        css(document.body, disableSelectStyle);
        document.addEventListener('mousemove', this.handleDrag);
        document.addEventListener('mouseup', this.handleDragEnd);
        document.onselectstart = returnFalse;
    }

    teardownDragging() {
        css(document.body, disableSelectStyleReset);
        document.removeEventListener('mousemove', this.handleDrag);
        document.removeEventListener('mouseup', this.handleDragEnd);
        document.onselectstart = undefined;
    }

    handleDragStart(event) {
        this.dragging = true;
        event.stopImmediatePropagation();
        this.setupDragging();
    }

    handleDrag(event) {
        if (this.prevPageX) {
            var { clientX } = event;
            var { left: trackLeft } = this.trackHorizontal.getBoundingClientRect();
            var thumbWidth = this.getThumbHorizontalWidth();
            var clickPosition = thumbWidth - this.prevPageX;
            var offset = -trackLeft + clientX - clickPosition;
            this.view.scrollLeft = this.getScrollLeftForOffset(offset);
        }
        if (this.prevPageY) {
            var { clientY } = event;
            var { top: trackTop } = this.trackVertical.getBoundingClientRect();
            var thumbHeight = this.getThumbVerticalHeight();
            var _clickPosition = thumbHeight - this.prevPageY;
            var _offset = -trackTop + clientY - _clickPosition;
            this.view.scrollTop = this.getScrollTopForOffset(_offset);
        }
        return false;
    }

    handleDragEnd() {
        this.dragging = false;
        this.prevPageX = this.prevPageY = 0;
        this.teardownDragging();
        this.handleDragEndAutoHide();
    }

    handleDragEndAutoHide() {
        var { autoHide } = this.props;
        if (!autoHide) return;
        this.hideTracks();
    }

    handleTrackMouseEnter() {
        this.trackMouseOver = true;
        this.handleTrackMouseEnterAutoHide();
    }

    handleTrackMouseEnterAutoHide() {
        var { autoHide } = this.props;
        if (!autoHide) return;
        this.showTracks();
    }

    handleTrackMouseLeave() {
        this.trackMouseOver = false;
        this.handleTrackMouseLeaveAutoHide();
    }

    handleTrackMouseLeaveAutoHide() {
        var { autoHide } = this.props;
        if (!autoHide) return;
        this.hideTracks();
    }

    showTracks() {
        clearTimeout(this.hideTracksTimeout);
        css(this.trackHorizontal, { opacity: 1 });
        css(this.trackVertical, { opacity: 1 });
    }

    hideTracks() {
        var _this2 = this;

        if (this.dragging) return;
        if (this.scrolling) return;
        if (this.trackMouseOver) return;
        var { autoHideTimeout } = this.props;
        clearTimeout(this.hideTracksTimeout);
        this.hideTracksTimeout = setTimeout(function () {
            css(_this2.trackHorizontal, { opacity: 0 });
            css(_this2.trackVertical, { opacity: 0 });
        }, autoHideTimeout);
    }

    detectScrolling() {
        var _this3 = this;

        if (this.scrolling) return;
        this.scrolling = true;
        this.handleScrollStart();
        this.detectScrollingInterval = setInterval(function () {
            if (_this3.lastViewScrollLeft === _this3.viewScrollLeft && _this3.lastViewScrollTop === _this3.viewScrollTop) {
                clearInterval(_this3.detectScrollingInterval);
                _this3.scrolling = false;
                _this3.handleScrollStop();
            }
            _this3.lastViewScrollLeft = _this3.viewScrollLeft;
            _this3.lastViewScrollTop = _this3.viewScrollTop;
        }, 100);
    }

    raf(callback) {
        var _this4 = this;

        if (this.requestFrame) raf.cancel(this.requestFrame);
        this.requestFrame = raf(function () {
            _this4.requestFrame = undefined;
            callback();
        });
    }

    update(callback) {
        var _this5 = this;

        this.raf(function () {
            return _this5._update(callback);
        });
    }

    _update(callback) {
        var { onUpdate, hideTracksWhenNotNeeded } = this.props;
        var values = this.getValues();
        if (getScrollbarWidth()) {
            var { scrollLeft, clientWidth, scrollWidth } = values;
            var trackHorizontalWidth = getInnerWidth(this.trackHorizontal);
            var thumbHorizontalWidth = this.getThumbHorizontalWidth();
            var thumbHorizontalX = scrollLeft / (scrollWidth - clientWidth) * (trackHorizontalWidth - thumbHorizontalWidth);
            var thumbHorizontalStyle = {
                width: thumbHorizontalWidth,
                transform: 'translateX(' + thumbHorizontalX + 'px)'
            };
            var { scrollTop, clientHeight, scrollHeight } = values;
            var trackVerticalHeight = getInnerHeight(this.trackVertical);
            var thumbVerticalHeight = this.getThumbVerticalHeight();
            var thumbVerticalY = scrollTop / (scrollHeight - clientHeight) * (trackVerticalHeight - thumbVerticalHeight);
            var thumbVerticalStyle = {
                height: thumbVerticalHeight,
                transform: 'translateY(' + thumbVerticalY + 'px)'
            };
            if (hideTracksWhenNotNeeded) {
                var trackHorizontalStyle = {
                    visibility: scrollWidth > clientWidth ? 'visible' : 'hidden'
                };
                var trackVerticalStyle = {
                    visibility: scrollHeight > clientHeight ? 'visible' : 'hidden'
                };
                css(this.trackHorizontal, trackHorizontalStyle);
                css(this.trackVertical, trackVerticalStyle);
            }
            css(this.thumbHorizontal, thumbHorizontalStyle);
            css(this.thumbVertical, thumbVerticalStyle);
        }
        if (onUpdate) onUpdate(values);
        if (typeof callback !== 'function') return;
        callback(values);
    }

    render() {
        var _this6 = this;

        var scrollbarWidth = getScrollbarWidth();
        /* eslint-disable no-unused-vars */
        var _props = this.props,
            {
            onScroll,
            onScrollFrame,
            onScrollStart,
            onScrollStop,
            onUpdate,
            renderView,
            renderTrackHorizontal,
            renderTrackVertical,
            renderThumbHorizontal,
            renderThumbVertical,
            tagName,
            hideTracksWhenNotNeeded,
            autoHide,
            autoHideTimeout,
            autoHideDuration,
            thumbSize,
            thumbMinSize,
            universal,
            autoHeight,
            autoHeightMin,
            autoHeightMax,
            style,
            children
        } = _props,
            props = _objectWithoutProperties(_props, ['onScroll', 'onScrollFrame', 'onScrollStart', 'onScrollStop', 'onUpdate', 'renderView', 'renderTrackHorizontal', 'renderTrackVertical', 'renderThumbHorizontal', 'renderThumbVertical', 'tagName', 'hideTracksWhenNotNeeded', 'autoHide', 'autoHideTimeout', 'autoHideDuration', 'thumbSize', 'thumbMinSize', 'universal', 'autoHeight', 'autoHeightMin', 'autoHeightMax', 'style', 'children']);
        /* eslint-enable no-unused-vars */

        var { didMountUniversal } = this.state;

        var containerStyle = _extends({}, containerStyleDefault, autoHeight && _extends({}, containerStyleAutoHeight, {
            minHeight: autoHeightMin,
            maxHeight: autoHeightMax
        }), style);

        var viewStyle = _extends({}, viewStyleDefault, {
            // Hide scrollbars by setting a negative margin
            marginRight: scrollbarWidth ? -scrollbarWidth : 0,
            marginBottom: scrollbarWidth ? -scrollbarWidth : 0
        }, autoHeight && _extends({}, viewStyleAutoHeight, {
            // Add scrollbarWidth to autoHeight in order to compensate negative margins
            minHeight: isString(autoHeightMin) ? 'calc(' + autoHeightMin + ' + ' + scrollbarWidth + 'px)' : autoHeightMin + scrollbarWidth,
            maxHeight: isString(autoHeightMax) ? 'calc(' + autoHeightMax + ' + ' + scrollbarWidth + 'px)' : autoHeightMax + scrollbarWidth
        }), autoHeight && universal && !didMountUniversal && {
            minHeight: autoHeightMin,
            maxHeight: autoHeightMax
        }, universal && !didMountUniversal && viewStyleUniversalInitial);

        var trackAutoHeightStyle = {
            transition: 'opacity ' + autoHideDuration + 'ms',
            opacity: 0
        };

        var trackHorizontalStyle = _extends({}, trackHorizontalStyleDefault, autoHide && trackAutoHeightStyle, (!scrollbarWidth || universal && !didMountUniversal) && {
            display: 'none'
        });

        var trackVerticalStyle = _extends({}, trackVerticalStyleDefault, autoHide && trackAutoHeightStyle, (!scrollbarWidth || universal && !didMountUniversal) && {
            display: 'none'
        });

        return createElement(tagName, _extends({}, props, { style: containerStyle, ref: function (ref) {
                _this6.container = ref;
            } }), [cloneElement(renderView({ style: viewStyle }), { key: 'view', ref: function (ref) {
                _this6.view = ref;
            } }, children), cloneElement(renderTrackHorizontal({ style: trackHorizontalStyle }), { key: 'trackHorizontal', ref: function (ref) {
                _this6.trackHorizontal = ref;
            } }, cloneElement(renderThumbHorizontal({ style: thumbHorizontalStyleDefault }), { ref: function (ref) {
                _this6.thumbHorizontal = ref;
            } })), cloneElement(renderTrackVertical({ style: trackVerticalStyle }), { key: 'trackVertical', ref: function (ref) {
                _this6.trackVertical = ref;
            } }, cloneElement(renderThumbVertical({ style: thumbVerticalStyleDefault }), { ref: function (ref) {
                _this6.thumbVertical = ref;
            } }))]);
    }
}

Scrollbars.propTypes = {
    onScroll: PropTypes.func,
    onScrollFrame: PropTypes.func,
    onScrollStart: PropTypes.func,
    onScrollStop: PropTypes.func,
    onUpdate: PropTypes.func,
    renderView: PropTypes.func,
    renderTrackHorizontal: PropTypes.func,
    renderTrackVertical: PropTypes.func,
    renderThumbHorizontal: PropTypes.func,
    renderThumbVertical: PropTypes.func,
    tagName: PropTypes.string,
    thumbSize: PropTypes.number,
    thumbMinSize: PropTypes.number,
    hideTracksWhenNotNeeded: PropTypes.bool,
    autoHide: PropTypes.bool,
    autoHideTimeout: PropTypes.number,
    autoHideDuration: PropTypes.number,
    autoHeight: PropTypes.bool,
    autoHeightMin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    autoHeightMax: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    universal: PropTypes.bool,
    style: PropTypes.object,
    children: PropTypes.node
};

Scrollbars.defaultProps = {
    renderView: renderViewDefault,
    renderTrackHorizontal: renderTrackHorizontalDefault,
    renderTrackVertical: renderTrackVerticalDefault,
    renderThumbHorizontal: renderThumbHorizontalDefault,
    renderThumbVertical: renderThumbVerticalDefault,
    tagName: 'div',
    thumbMinSize: 30,
    hideTracksWhenNotNeeded: false,
    autoHide: false,
    autoHideTimeout: 1000,
    autoHideDuration: 200,
    autoHeight: false,
    autoHeightMin: 0,
    autoHeightMax: 200,
    universal: false
};