export default function getInnerWidth(el) {
    var { clientWidth } = el;
    var { paddingLeft, paddingRight } = getComputedStyle(el);
    return clientWidth - parseFloat(paddingLeft) - parseFloat(paddingRight);
}