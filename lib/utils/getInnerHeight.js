export default function getInnerHeight(el) {
    var { clientHeight } = el;
    var { paddingTop, paddingBottom } = getComputedStyle(el);
    return clientHeight - parseFloat(paddingTop) - parseFloat(paddingBottom);
}